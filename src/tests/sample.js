const chromeDriver = require("../drivers/chrome");
const {Builder, By} = require("selenium-webdriver");


 

describe("Aura Code Challenge - Create User Account Tests", () => {
  let driver;
  beforeAll(() => {
      driver = new Builder().forBrowser("chrome").build();
      driver.manage().window().maximize();
  });

  afterAll(async () => {
    await driver.quit();
  });

 

  test("it loads authentication page", async () => {
    await driver.get(
      "http://automationpractice.com/index.php?controller=authentication&back=my-account"
    );
  
    const title = await driver.getTitle();
    expect(title).toBe("Login - My Store");

    await driver.findElement(By.id('email_create')).sendKeys("ksrkpdba5@gmail.com");
    await driver.findElement(By.id('SubmitCreate')).click();
	
	// Here we have to check your account information page submit form and enter the all details into text box fields
	
      await driver.sleep(5000).then (async function() {           
      await driver.findElement(By.xpath('(//*[@class="page-subheading"])[1]')).getText().then(async function(text) {
      expect(text).toBe("YOUR PERSONAL INFORMATION");
	  
	  await driver.findElement(By.name('id_gender')).click();

      await driver.findElement(By.name('customer_firstname')).sendKeys("Varma");
      await driver.findElement(By.name('customer_lastname')).sendKeys("Kola");
	  
	  await driver.findElement(By.name('passwd')).sendKeys("123456");
	  
	  
	  await driver.findElement(By.xpath('//*[@id="days"]/option[26]')).click();
      await driver.findElement(By.xpath('//*[@id="months"]/option[7]')).click();
      await driver.findElement(By.xpath('//*[@id="years"]/option[30]')).click();
	  
	  await driver.findElement(By.xpath('//*[@id="company"]')).sendKeys("TSSSINFOTECH");
	  await driver.findElement(By.xpath('//*[@id="address1"]')).sendKeys("H.NO:6-9-45, ChandaNagar");
	  
	  await driver.findElement(By.xpath('//*[@id="city"]')).sendKeys("Hyderabad");
	  await driver.findElement(By.xpath('//*[@id="id_state"]/option[5]')).click();
	  await driver.findElement(By.xpath('//*[@id="postcode"]')).sendKeys("50085");
	  
	  await driver.findElement(By.xpath('//*[@id="phone_mobile"]')).sendKeys("7981086877");
	  await driver.findElement(By.xpath('//*[@id="alias"]')).clear();
	  
	  await driver.findElement(By.xpath('//*[@id="alias"]')).sendKeys("Madhapur,Hyderabad");
	  
	  await driver.findElement(By.xpath('//*[@id="submitAccount"]/span/i')).click();
	  
	 //After successfull account created we have to check title of account information page
	  const title = await driver.getTitle();
      expect(title).toBe("My account - My Store");
	  
	  });
	  });
	 
  });	 
  });